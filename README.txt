This module allows running commands via a form.

It is meant to be used only in development environments in
closed networks since it opens the gates of hell.

Installation
============

Install the module using Drush or the Drupal admin interface.

Usage
=====

In a web browser, authenticate as administrator and then navigate
to /command-runner-ui/run, where there is a form to enter the
command to run.
